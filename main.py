import driving_test_crawler as dtc
import datetime
from twilio.rest import Client
import time

import credentials


def notify(message):
    account_sid = credentials.ACCOUNT_SID
    auth_token = credentials.AUTH_TOKEN
    client = Client(account_sid, auth_token)

    # this is the Twilio sandbox testing number
    from_whatsapp_number = 'whatsapp:+14155238886'
    to_whatsapp_number = credentials.WHATSAPP_NUMBER

    client.messages.create(body=message,
                           from_=from_whatsapp_number,
                           to=to_whatsapp_number)


if __name__ == "__main__":
    
    CURRENT_TEST_DATE = datetime.datetime(2019, 12, 4, 2, 12)

    datetime_unavailable = ['2019-12-10 07:50:00']
    date_unavailable = ['2019-11-14', '2019-11-21', '2019-11-28', '2019-12-05', '2019-12-12', '2019-12-19']
    time_unavailable = []

    while True:
        crawler = dtc.DrivingTestCrawler()
        crawler.crawl()

        earlier_dates = {k: v for k, v in crawler.slots.items() if
                         datetime.datetime.fromtimestamp(int(v) / 1000) < CURRENT_TEST_DATE and
                         datetime.datetime.fromtimestamp(int(v) / 1000).strftime("%Y-%m-%d %H:%M:%S") not in datetime_unavailable and
                         datetime.datetime.fromtimestamp(int(v) / 1000).strftime("%Y-%m-%d") not in date_unavailable and
                         datetime.datetime.fromtimestamp(int(v) / 1000).strftime("%H:%M:%S") not in time_unavailable}

        if earlier_dates:
            for (elem, epoch) in earlier_dates.items():
                message = 'DRIVING TEST SLOT AVAILABLE AT {0}'.format(datetime.datetime.fromtimestamp(int(epoch)/1000).strftime('%c'))
                notify(message)
                print(message)

        crawler.tearDown()
        time.sleep(20)
