# Driving Test Cancellation Notifier
A small Python program that logs a user into the DVSA test website to change a test date and scrapes any dates with available slots. The User specifies their current test date and if there is a cancellation before this date it will send a text notification with the date(s) and time(s) via Whatsapp.


It will only work if you already have a booked test. In the futute it would be possible to book the cancellation automatically.

The DVSA website shuts down from 11pm(?)-6am so will require a restart of the program. The website may also have undergone some changes with HTML and the crawler may not work.


# Usage

Create a 'credentials.py' file using the 'credentials_template.py' and input correct data.

Open 'main.py' and input datetime, dates, or times unavailable. The noifier will not notify if these slots are available.

Run 'main.py'



