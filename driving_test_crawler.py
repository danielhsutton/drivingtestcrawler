from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from fake_useragent import UserAgent, FakeUserAgentError

import credentials


class DrivingTestCrawler:
    name = "driving_test_spider"
    start_url = 'https://www.gov.uk/change-driving-test'
    allowed_domains = ['www.gov.uk']

    def __init__(self):
        self.options = FirefoxOptions()
        self.options.add_argument('-headless')

        binary = FirefoxBinary('/usr/bin/firefox-esr')

        try:
            ua = UserAgent()
        except FakeUserAgentError:
            pass

        useragent = ua.random
        self.options.add_argument(f'user-agent={useragent}')

        self.driver = webdriver.Firefox(firefox_binary=binary, firefox_options=self.options, executable_path='/usr/local/share/gecko_driver/geckodriver')

        self.driver.set_window_size(1500, 1000)
        self.driver.get(self.start_url)

        # list of all available slots found
        self.slots = {}

    def click(self, xpath):
        next_url = self.driver.find_element_by_xpath(xpath)
        try:
            # click the button to go to next page
            next_url.click()
        except Exception as e:
            # Just print(e) is cleaner and more likely what you want,
            # but if you insist on printing message specifically whenever possible...
            if hasattr(e, 'message'):
                print(e.message)
            else:
                print(e)

    def input(self, id, input):
        input_element = self.driver.find_element_by_id(id)
        try:
            # click the button to go to next page
            input_element.send_keys(input)
        except Exception as e:
            print(e)

    # Returns a list of epoch time values in seconds. Epochs refer to time slot of all available dates
    def get_available_slots(self):
        date_elements = self.driver.find_elements_by_class_name('BookingCalendar-date--bookable')

        for elem in date_elements:
            date_element = elem.find_element_by_class_name('BookingCalendar-dateLink')
            date_value = date_element.get_attribute('data-date')
            time_slot_elements = self.driver.find_element_by_id(f'date-{date_value}').find_elements_by_class_name('SlotPicker-slot')

            for time_slot_elem in time_slot_elements:
                epoch = time_slot_elem.get_attribute('value')
                self.slots[time_slot_elem] = epoch

    def crawl(self):
        self.click('/html/body/div[6]/div[1]/main/div/div/div/section[1]/p/a')
        self.input('driving-licence-number', credentials.LICENSE_NO)
        self.input('application-reference-number', credentials.REF_NO)
        self.click('//*[@id="booking-login"]')


        self.click('//*[@id="date-time-change"]')
        self.click('//*[@id="test-choice-earliest"]')
        self.click('//*[@id="driving-licence-submit"]')

        self.get_available_slots()

    def tearDown(self):
        self.driver.quit()

    def screenshot(self):
        self.driver.save_screenshot("screenshot.png")
